import numpy as np
from skimage.transform import resize
import cv2


def bgr2gray(arr):
    g = np.dot(arr[..., :3], [0.299, 0.587, 0.114])
    # print(g)
    return g


def downsample(img, oshape):
    return resize(img, oshape)


def crop(img, x1, x2, y1, y2):
    return img[x1:x2, y1:y2]


# good theta for Ms Pacman to capture who game field
def ms_pacman(img):
    b = downsample(bgr2gray(img), (100, 84))
    return crop(b, 0, 84, 0, b.shape[1]).astype(np.uint8)


# captures img for breakout
def breakout(img):
    b = downsample(bgr2gray(img), (100, 84))
    return crop(b, 16, 100, 0, b.shape[1]).astype(np.uint8)

def gridword(img):
    return cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)