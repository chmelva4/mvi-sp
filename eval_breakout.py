from network import *
import gym
from agents import *
from preproc import breakout
import time


if __name__ == "__main__":
    with tf.Session() as session:


        network = DoubleDQNSimpleBothNetworks(4, tf.train.RMSPropOptimizer(0.0001), 0.99)
        env = gym.make('Breakout-v0')
        saver = tf.train.Saver()
        saver.restore(session, "./models/ddqn_breakout_fin-1000.ckpt")
        env.render()
        time.sleep(5)
        ra = BreakoutAgentRandom(env)
        print(ra.evaluate(10))

        na = BreakoutAgent(env, network, session, breakout)
        print(na.evaluate(10))