# Užití neuronových sítí k hraní Atari her

## Zadání

Prostudujte si algoritmy vhodné k vytvoření AI, která zvládne hrát hry na Atari

 

- policy gradients (nekolik ruznych metod)
- deep q-network (DeepMind)
- a3c
- rainbow deep q-network

 

Jeden algoritmus si vyberte a naimplementuje tak, aby zvládl hrát aspoň jednu Atari hru.

Předzpracováním dat se rozumí zprovoznění OpenAI Gym, extrakci dat.



## Instalace prostředí

Všechy závislosti nainstalujeme příkazem:

```python
pip install tensorflow-gpu scikit-image opencv-python gym
pip install gym['atari']
```

Všechno trénování jsem dělal na grafice a nemám otestováno, jakým způsobem to běží na CPU

## Mapa repozitáře

* train_gridworld.py - spustí trénovaní gridworld
* train_breakout.py - spustí trénovaní breakout
* eval_gridworld.py - spustí evaulaci gridworld v pruměru 10 her - nazev je hardcoded string
* eval_breakout.py - spustí evaulaci breakout v pruměru 10 her - nazev je hardcoded string
* dqn.py  - obsahuje fce potřebné 'k běhu dqn
* gridworld.py - obsahuje prostředí gridworld
* agents.py - třídy nutné k evaulaci
* memory.py - obsahuje experience replay buffer
* network.py - obsahuje grafy neuronovych sítí
* preproc.py - obsahuje fce nutné k předzpracování dat