import tensorflow as tf
import gym
import time

from memory import *
from preproc import breakout, gridword
from network import *
import dqn
from collections import deque
from gridworld import gameEnv
import os

# episodes_num = 1000
env = gym.make('BreakoutDeterministic-v4')

total_frames = 1000000
eps = 1
eps_step = 0.9 / total_frames
eps_treshold = 0.1

mem_size = 300000
mem = ReplayMemory(mem_size)

discount_factor = 0.99
batch_size = 32


theta = breakout
# ep_count = 100
model_name = './models/ddqn_breakout_final'
log_path = "ddqn_breakout_final.txt"
best_result = 0

update_target_network_frames = 2500
save_eps = 1000

# network = NNetwork(4, tf.train.GradientDescentOptimizer(learning_rate), )

avg_deque = deque()
avg_q_deque = deque()
sum_100 = 0
q_100 = 0
count_100 = 0


def updateTargetGraph(tfVars):
    total_vars = len(tfVars)
    op_holder = []
    for idx,var in enumerate(tfVars[0:total_vars//2]):
        op_holder.append(tfVars[idx+total_vars//2].assign(var.value()))
    return op_holder

if __name__ == '__main__':
    ##main algorithm loop

    with tf.Session() as session:
        network = DoubleDQNSimpleBothNetworks(4, tf.train.RMSPropOptimizer(0.00025), 0.99)
        saver = tf.train.Saver()
        trainables = tf.trainable_variables()

        network.up_ops = updateTargetGraph(trainables)

        session.run(tf.global_variables_initializer())
        try:
            pass
            # saver.restore(session, './breakout-500.ckpt')
        except tf.errors.NotFoundError:
            print("no checkpoint found")

        s = env.reset()

        total_reward = 0
        total_q = 0
        total_loss = 0
        total_frame_count = 0
        frame_count_delta = 0
        episode = 0
        avg_reward = 0
        with open(log_path, 'w') as file:
            dqn.populate_memory(mem, env, theta, 50000)
            print("done populating mem")
            # network.update_target_network(session)
            while True:
                print("Running episode {}".format(episode))
                start_time = time.time()
                reward, frame_count, eps, ep_avg_q, ep_loss = dqn.train_episode(session, env, network, mem, theta,
                                                             dqn.train_from_experience_two_networks_dqn,
                                                             eps, eps_step, eps_treshold,
                                                             batch_size, episode % 10 == 0)
                if len(avg_deque) < 100:
                    avg_deque.appendleft(reward)
                    avg_q_deque.appendleft(ep_avg_q)
                    sum_100 += reward
                    q_100 += ep_avg_q
                    count_100 += 1
                else:
                    sub = avg_deque.pop()
                    subq = avg_q_deque.pop()
                    sum_100 -= sub
                    q_100 -= subq
                    sum_100 += reward
                    q_100 += ep_avg_q
                    avg_deque.appendleft(reward)
                    avg_q_deque.appendleft(ep_avg_q)


                end_time = time.time()
                total_frame_count += frame_count
                total_q += ep_avg_q
                total_loss += ep_loss
                frame_count_delta += frame_count
                secs = end_time - start_time
                print("episode took {}s at {} fps".format(secs,
                                                           frame_count/secs))
                total_reward += reward
                if reward > best_result:
                    best_result = reward
                avg_reward = total_reward/float(episode + 1)
                log_str = "episode {} total reward {}, avg reward {}, average last 100 {}, frame_count {}, best result {}," \
                          " total fc {}, ep avg q {}, total avg q {}, last 100 avg q {} ep loss, {}, avg loss {}"\
                    .format(episode + 1, reward, avg_reward, float(sum_100)/count_100, frame_count, best_result,
                            total_frame_count, ep_avg_q, total_q / float(episode + 1), float(q_100) / count_100, ep_loss, total_loss/ float(episode + 1))
                print(log_str)
                file.write("{}\n".format(log_str))

                if frame_count_delta > update_target_network_frames:
                    network.update_target_network(session)
                    print("updated target network".format(total_frames))
                    frame_count_delta -= update_target_network_frames
                if episode % save_eps == 0:
                    print("saving model after {} episodes".format(episode))
                    saver.save(session, "{}-{}.ckpt".format(model_name, episode))
                if float(sum_100/count_100) > 20:
                    break
                episode += 1

        saver.save(session, "{}-{}-final.ckpt".format(model_name, episode - 1))
