import numpy as np
import cv2
import random

class Agent:

    def __init__(self, env):
        self.env = env

    def __play_episode(self):
        s = self.env.reset()
        total_reward = 0
        done = False
        while not done:
            action = self._choose_action(s)
            s, r, done, i = self.env.step(action)
            total_reward += r
        return total_reward

    def evaluate(self, num_episodes):
        reward = 0
        for i in range(num_episodes):
            reward += self.__play_episode()
        return float(reward) / num_episodes

    def _choose_action(self, s):
        raise NotImplementedError


class RandomAgent(Agent):

    def _choose_action(self, s):
        return self.env.action_space.sample()

class GridWorldAgentrandom(RandomAgent):

    def __init__(self, env):
        super().__init__(env)
        self.win = cv2.namedWindow("game", cv2.WINDOW_NORMAL)
        cv2.resizeWindow('game', 600, 600)


    def _choose_action(self, s):
        self.env.render(100, "game")
        return super()._choose_action(s)

class BreakoutAgentRandom(RandomAgent):


    def _choose_action(self, s):
        self.env.render()
        return super()._choose_action(s)


class GridWorldAgent(Agent):

    def __init__(self, env, network, session):
        super().__init__(env)
        self.network = network
        self.session = session
        self.win = cv2.namedWindow("game", cv2.WINDOW_NORMAL)
        cv2.resizeWindow('game', 600, 600)


    def _choose_action(self, s):
        self.env.render(100, "game")
        action = self.session.run(self.network.output,
                             feed_dict={self.network.input: s.reshape(1, 84, 84, 3)})
        return np.argmax(action)


class NNAgent(Agent):

    def __init__(self, env, network, session, preproc):
        self.env = env
        self.network = network
        self.session = session
        self.preproc = preproc
        self.frames = [
            self.preproc(env.reset()),
            self.preproc(env.step(0)[0]),
            self.preproc(env.step(1)[0]),
            self.preproc(env.step(2)[0])
        ]
        self.index = 0


    def _choose_action(self, s):
        self.env.render()
        self.frames[self.index] = self.preproc(s)
        self.index += 1
        if self.index == 4:
            self.index = 0
        tensor = np.stack(self.frames, axis=2).astype(np.float32)
        action = self.session.run(self.network.output,
                             feed_dict={self.network.input: tensor.reshape(1, 84, 84, 4)})
        return np.argmax(action)


class BreakoutAgent:

    def __init__(self, env, network, session, preproc):
        self.env = env
        self.network = network
        self.session = session
        self.preproc = preproc
        self.frames = [
            self.preproc(env.reset()),
            self.preproc(env.step(0)[0]),
            self.preproc(env.step(1)[0]),
            self.preproc(env.step(2)[0])
        ]
        self.index = 0

    def _choose_action(self, s):

        self.env.render()
        self.frames[self.index] = self.preproc(s)
        self.index += 1
        if self.index == 4:
            self.index = 0
        if random.random() < 0.0025:
            return 1
        tensor = np.stack(self.frames, axis=2).astype(np.float32)
        action = self.session.run(self.network.output,
                                  feed_dict={self.network.input: tensor.reshape(1, 84, 84, 4)})
        return np.argmax(action)

    def __play_episode(self):
        s = self.env.reset()
        s, r, d, i = self.env.step(1)
        total_reward = 0
        done = False
        while not done:
            action = self._choose_action(s)
            s, r, done, i = self.env.step(action)
            total_reward += r
        return total_reward

    def evaluate(self, num_episodes):
        reward = 0
        for i in range(num_episodes):
            reward += self.__play_episode()
        return float(reward) / num_episodes