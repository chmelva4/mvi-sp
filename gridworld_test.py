from gridworld import gameEnv
import random
from preproc import gridword, bgr2gray
import cv2



if __name__ == '__main__':
    env = gameEnv(False, 5, 50)
    s = env.reset()

    for i in range(10):
        s = env.reset()
        episode_reward = 0
        step = 0
        while True:
            env.render()
            # cv2.imshow('preproc', gridword(s)), cv2.waitKey(100)
            a = random.randrange(0, 5)
            s, r, d = env.step(a)
            episode_reward += r
            # print(r)
            if d:
                break
            step += 1
        print(episode_reward)