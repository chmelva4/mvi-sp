from network import *
from gridworld import gameEnv
from agents import *
from preproc import gridword


if __name__ == "__main__":
    with tf.Session() as session:


        network = DoubleDuelingDQNGridworld(4, tf.train.AdamOptimizer(0.0001), 0.99, 0.0001)
        env = gameEnv(False, 5, 50)
        saver = tf.train.Saver()
        saver.restore(session, "./models/dddqn_gridworld_ff-999-final.ckpt")

        ra = GridWorldAgentrandom(env)
        print(ra.evaluate(10))

        na = GridWorldAgent(env, network, session)
        print(na.evaluate(10))