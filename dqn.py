import random
import numpy as np
from memory import Experience
# from tensorflow.python.client import timeline

def __get_parsed_data_from_mem(mem, batch_size):
    replays = mem.sample_memories(batch_size)
    future_states = []
    start_states = []
    rewards = []
    dones = []
    actions = []
    for rep in replays:
        future_states.append(np.stack(rep.s1, axis=2).astype(np.float32))
        start_states.append(np.stack(rep.s0, axis=2).astype(np.float32))
        rewards.append(rep.r)
        dones.append(rep.d)
        actions.append(rep.a)
    return start_states, future_states, rewards, dones, actions


def choose_action(env, network, session, frames, eps):
    if random.random() < eps:
        return env.action_space.sample()
    else:
        tensor = np.stack(frames, axis=2).astype(np.float32)
        action = session.run(network.output,
                             feed_dict={network.input: tensor.reshape(1, 84, 84, 4)})
        return np.argmax(action)


def process_reward(reward):
    if reward > 0:
        return 1
    if reward < 0:
        return -1
    return 0


def populate_memory(memory, env, theta,  count):
    percent = int(count/100)
    state = env.reset()
    for i in range(4):
        memory.append_frame(theta(state))
    frames = memory.get_last_4_frames()

    for i in range(count):
        action = env.action_space.sample()
        new_image, reward, done, info = env.step(action)

        memory.append_frame(theta(new_image))
        new_frames = memory.get_last_4_frames()
        memory.append(Experience(frames, action, reward, 0 if done else 1, new_frames))
        frames = new_frames

        if done:
            env.reset()
        if i % percent == 0:
            print("mem progress {} %".format(i/percent))

def populate_memory_gridworld(memory, env, theta,  count):
    percent = int(count/100)
    state = env.reset()
    for i in range(count):
        action = env.action_space.sample()
        new_image, reward, done, info = env.step(action)
        memory.append(Experience(state, action, reward, 0 if done else 1, new_image))

        state = new_image

        if done:
            env.reset()
        if i % percent == 0:
            print("mem progress {} %".format(i/percent))


def train_from_experience_simple_dqn(session, network, memory, batch_size):
    start_states, future_states, rewards, dones, actions = __get_parsed_data_from_mem(memory, batch_size)

    yj = session.run(
        network.yj,
        feed_dict={
            network.input: future_states,
            network.done_pl: dones,
            network.reward_pl: rewards,
        })

    s = session.run([
                    network.train
                    ],
                    feed_dict={
                        network.input: start_states,
                        network.actions_pl: actions,
                        network.yj_pl: yj
                    })


def train_from_experience_two_networks_dqn(session, network, memory, batch_size):
    start_states, future_states, rewards, dones, actions = __get_parsed_data_from_mem(memory, batch_size)
    double_q = session.run(network.target_vals,
                           feed_dict={network.input: future_states, network.t_input: future_states})

    s = session.run([
        network.avg_q,
        network.loss,
        network.train
    ],
        feed_dict={
            network.input: start_states,
            network.actions_pl: actions,
            network.reward_pl: rewards,
            network.done_pl: dones,
            network.doubleq_pl: double_q
        })
    return s[0], s[1]


def train_from_experience_two_networks_dqn_gridworld(session, network, memory, batch_size):
    replays = memory.sample_memories(batch_size)
    future_states = []
    start_states = []
    rewards = []
    dones = []
    actions = []
    for rep in replays:
        future_states.append(rep.s1)
        start_states.append(rep.s0)
        rewards.append(rep.r)
        dones.append(rep.d)
        actions.append(rep.a)

    double_q = session.run(network.target_vals, feed_dict = {network.input:future_states, network.t_input:future_states})
    # target_q = session.run(network.t_output, feed_dict = {network.t_input:future_states})

    # double_q = target_q[range(batch_size), predicts]

    s = session.run([
                    network.avg_q,
                    network.loss,
                    network.train
                    ],
                    feed_dict={
                        network.input: start_states,
                        network.actions_pl: actions,
                        network.reward_pl: rewards,
                        network.done_pl: dones,
                        network.doubleq_pl: double_q
                    })
    return s[0], s[1]


def train_episode(session, env, network, memory, theta, train_function, eps, eps_step, eps_treshold, batch_size, should_render):
    total_reward = 0
    sum_q = 0
    sum_loss = 0
    frame_count = 0
    state = env.reset()
    for i in range(4):
        memory.append_frame(theta(state))
    frames = memory.get_last_4_frames()
    done = 1
    # env.step(1)
    action = 0

    while done == 1:
        if should_render:
            env.render()
        # choose action either randomly or as an output of Neural Network
        action = choose_action(env, network, session, frames, eps)
        # print("chosen action is {}".format(action))

        if eps > eps_treshold:
            eps -= eps_step

            # print("eps is {}".format(eps))

        new_image, reward, done, info = env.step(action)

        done = 0 if done else 1

        reward = process_reward(reward)
        # print(reward)
        total_reward += reward

        memory.append_frame(theta(new_image))
        new_frames = memory.get_last_4_frames()
        memory.append(Experience(frames, action, reward, done, new_frames))

        frames = new_frames

        avg_q, avg_loss = train_function(session, network, memory, batch_size)

        sum_q += avg_q
        sum_loss += avg_loss

        frame_count += 1

    return total_reward, frame_count, eps, sum_q / float(frame_count), sum_loss / float(frame_count)


def choose_action_gridworld(env, network, session, state, eps):
    # r = random.random()
    # print(r, eps)
    if random.random() < eps:
        return env.action_space.sample()
    else:
        tensor = state.astype(np.float32)
        action = session.run(network.output,
                             feed_dict={network.input: tensor.reshape(1, 84, 84, 3)})
        a = np.argmax(action)
        return a


def train_episode_gridworld(session, env, network, memory, theta, train_function, eps, eps_step, eps_treshold, batch_size, should_render):
    total_reward = 0
    sum_q = 0
    sum_loss = 0
    frame_count = 0
    state = env.reset()
    done = 1

    while done == 1:
        if should_render:
            env.render()
        # choose action either randomly or as an output of Neural Network
        action = choose_action_gridworld(env, network, session, state, eps)
        # print("chosen action is {}".format(action))


        # env.render()
        if eps > eps_treshold:
            eps -= eps_step

            # print("eps is {}".format(eps))

        new_image, reward, done, info = env.step(action)

        done = 0 if done else 1

        reward = process_reward(reward)
        # print(reward)
        total_reward += reward

        memory.append(Experience(state, action, reward, done, new_image))

        state = new_image


        if frame_count % 4 == 0:
            q, loss = train_function(session, network, memory, batch_size)
            sum_q += q
            sum_loss += loss
            network.update_target_network(session)

        frame_count += 1

    return total_reward, frame_count, eps, sum_q/ float(50/4), sum_loss / float(50/4)
