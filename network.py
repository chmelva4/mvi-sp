import tensorflow as tf
import numpy as np


class DQNSimpleOneNetwork:
    tf.reset_default_graph()

    def __define_architecture(self, num_actions):
        self.input = tf.placeholder(tf.float32, shape=(None, 84, 84, 4))
        self.c1 = tf.layers.Conv2D(16, 8, 4, activation=tf.nn.relu)
        self.c2 = tf.layers.Conv2D(32, 4, 2, activation=tf.nn.relu)
        # self.fl = tf.layers.flatten(self.c2)
        self.h = tf.layers.Dense(256, tf.nn.relu)
        self.output_layer = tf.layers.Dense(num_actions)

    def __connect_layers(self):
        self.c1o = self.c1.apply(self.input)
        self.c2o = self.c2.apply(self.c1o)
        self.fl = tf.layers.flatten(self.c2o)
        self.ho = self.h.apply(self.fl)
        self.output = self.output_layer.apply(self.ho)

    def __init__(self, num_actions, optimizer, future_discount):
        # TF remembers everything you defined, this will keep the computation graph clean
        self.__define_architecture(num_actions)
        self.__connect_layers()

        self.optimizer = optimizer
        # stacked last 4 frames of the game


        self.reward_pl = tf.placeholder(tf.float32)
        self.done_pl = tf.placeholder(tf.float32)
        self.actions_pl = tf.placeholder(tf.int32, shape=[None])
        self.future_discount = tf.constant(future_discount)

        self.reduce_targets = tf.reduce_max(self.output, axis=1)

#         yj = rj + done * gammma * maximum
        self.yj = tf.add(self.reward_pl,
                         tf.multiply(self.future_discount,
                                     tf.multiply(self.done_pl, self.reduce_targets)
                                     )
                         )
        self.hots = tf.one_hot(self.actions_pl, num_actions, 1.0, 0.0, dtype=tf.float32)
        self.yj_pl = tf.placeholder(tf.float32)
        self.yj_exp = tf.expand_dims(self.yj_pl, axis=1)
        self.yj_mul = tf.concat([self.yj_exp for i in range(num_actions)], axis=1)
        self.yj_masked = tf.multiply(self.yj_mul, self.hots)
        self.predictions = tf.multiply(self.output, self.hots)
        self.loss = tf.losses.mean_squared_error(self.yj_masked, self.predictions)
        self.train = optimizer.minimize(self.loss)


class DQNSimpleBothNetworks:
    tf.reset_default_graph()

    def __define_architecture(self, num_actions):
        # main network
        self.input = tf.placeholder(tf.float32, shape=(None, 84, 84, 4))
        self.c1 = tf.layers.Conv2D(16, 8, 4, activation=tf.nn.relu)
        self.c2 = tf.layers.Conv2D(32, 4, 2, activation=tf.nn.relu)
        # self.fl = tf.layers.flatten(self.c2)
        self.h = tf.layers.Dense(256, tf.nn.relu)
        self.output_layer = tf.layers.Dense(num_actions)

        # target network
        self.t_input = tf.placeholder(tf.float32, shape=(None, 84, 84, 4))
        self.t_c1 = tf.layers.Conv2D(16, 8, 4, activation=tf.nn.relu, trainable=False)
        self.t_c2 = tf.layers.Conv2D(32, 4, 2, activation=tf.nn.relu, trainable=False)
        # self.fl = tf.layers.flatten(self.c2)
        self.t_h = tf.layers.Dense(256, tf.nn.relu, trainable=False)
        self.t_output_layer = tf.layers.Dense(num_actions, trainable=False)

    def __connect_layers(self):
        self.c1o = self.c1.apply(self.input)
        self.c2o = self.c2.apply(self.c1o)
        self.fl = tf.layers.flatten(self.c2o)
        self.ho = self.h.apply(self.fl)
        self.output = self.output_layer.apply(self.ho)

        self.t_c1o = self.t_c1.apply(self.t_input)
        self.t_c2o = self.t_c2.apply(self.t_c1o)
        self.t_fl = tf.layers.flatten(self.t_c2o)
        self.t_ho = self.t_h.apply(self.t_fl)
        self.t_output = self.t_output_layer.apply(self.t_ho)

    def __init__(self, num_actions, optimizer, future_discount):
        # TF remembers everything you defined, this will keep the computation graph clean
        self.__define_architecture(num_actions)
        self.__connect_layers()

        self.optimizer = optimizer
        # stacked last 4 frames of the game


        self.reward_pl = tf.placeholder(tf.float32)
        self.done_pl = tf.placeholder(tf.float32)
        self.actions_pl = tf.placeholder(tf.int32, shape=[None])
        self.future_discount = tf.constant(future_discount)

        self.reduce_targets = tf.reduce_max(self.t_output, axis=1)

#         yj = rj + done * gammma * maximum
        self.yj = tf.add(self.reward_pl,
                         tf.multiply(self.future_discount,
                                     tf.multiply(self.done_pl, self.reduce_targets)
                                     )
                         )
        self.hots = tf.one_hot(self.actions_pl, num_actions, 1.0, 0.0, dtype=tf.float32)
        self.yj_exp = tf.expand_dims(self.yj, axis=1)
        self.yj_mul = tf.concat([self.yj_exp for i in range(num_actions)], axis=1)
        self.yj_masked = tf.multiply(self.yj_mul, self.hots)
        self.predictions = tf.multiply(self.output, self.hots)
        self.loss = tf.losses.mean_squared_error(self.yj_masked, self.predictions)
        # sess.run(tf.variables_initializer())
        # self.init_train_vars()
        self.train = optimizer.minimize(self.loss)

    def update_target_network(self, sess):
        # note that the zero is necessary to create copy
        # trainable_weights[0] - weights, [1] biases
        4

    # def init_train_vars(self):
    #     self.main_vars = [
    #         self.c1.trainable_weights[0], self.c1.trainable_weights[1],
    #         self.c2.trainable_weights[0], self.c2.trainable_weights[1],
    #         self.h.trainable_weights[0], self.h.trainable_weights[1],
    #         self.output.trainable_weights[0], self.output.trainable_weights[1]
    #     ]


class DoubleDQNSimpleBothNetworks:
    tf.reset_default_graph()

    def __define_architecture(self, num_actions):
        # main network
        self.input = tf.placeholder(tf.float32, shape=(None, 84, 84, 4))
        self.c1 = tf.layers.conv2d(self.input, 16, 8, 4, activation=tf.nn.relu)
        self.c2 = tf.layers.conv2d(self.c1, 32, 4, 2, activation=tf.nn.relu)
        self.fl = tf.layers.flatten(self.c2)
        self.dense = tf.layers.dense(self.fl, 256, tf.nn.relu)
        self.output = tf.layers.dense(self.dense, num_actions)

        self.t_input = tf.placeholder(tf.float32, shape=(None, 84, 84, 4))
        self.t_c1 = tf.layers.conv2d(self.t_input, 16, 8, 4, activation=tf.nn.relu)
        self.t_c2 = tf.layers.conv2d(self.t_c1, 32, 4, 2, activation=tf.nn.relu)
        self.t_fl = tf.layers.flatten(self.t_c2)
        self.t_dense = tf.layers.dense(self.t_fl, 256, tf.nn.relu)
        self.t_output = tf.layers.dense(self.t_dense, num_actions)

    def __init__(self, num_actions, optimizer, future_discount):
        # TF remembers everything you defined, this will keep the computation graph clean
        self.__define_architecture(num_actions)
        self.optimizer = optimizer

        self.reward_pl = tf.placeholder(tf.float32)
        self.done_pl = tf.placeholder(tf.float32)
        self.actions_pl = tf.placeholder(tf.int32, shape=[None])
        self.doubleq_pl = tf.placeholder(tf.float32, shape=[None])
        self.future_discount = tf.constant(future_discount)

        self.reduce_targets = tf.argmax(self.output, axis=1)

        self.bool_hot = tf.one_hot(self.reduce_targets, num_actions, True, False)
        self.target_vals = tf.boolean_mask(self.t_output, self.bool_hot)

        #         yj = rj + done * gammma * maximum
        self.yj = tf.add(self.reward_pl,
                         tf.multiply(self.future_discount,
                                     tf.multiply(self.done_pl, self.doubleq_pl)
                                     )
                         )
        self.hots = tf.one_hot(self.actions_pl, num_actions, 1.0, 0.0, dtype=tf.float32)

        self.predictions = tf.reduce_sum(tf.multiply(self.output, self.hots), axis=1)
        self.loss = tf.losses.huber_loss(self.yj, self.predictions)
        self.avg_q = tf.reduce_mean(self.output)
        self.train = optimizer.minimize(self.loss)

    def update_target_network(self, sess):
            sess.run(self.up_ops)

class DoubleDuelingDQNGridworld:
    tf.reset_default_graph()

    def __define_architecture(self, num_actions):
        # main network
        # use rgb as input
        self.input = tf.placeholder(tf.float32, shape=(None, 84, 84, 3))

        self.c1 = tf.layers.conv2d(self.input ,32, 8, 4, use_bias=False, activation=tf.nn.relu)
        self.c2 = tf.layers.conv2d(self.c1, 64, 4, 2, use_bias=False, activation=tf.nn.relu)
        self.c3 = tf.layers.conv2d(self.c2, 64, 3, 1, use_bias=False, activation=tf.nn.relu)
        self.c4 = tf.layers.conv2d(self.c3, 512, 7, 1, use_bias=False, activation=tf.nn.relu)

        self.streamA, self.streamV = tf.split(self.c4, 2, 3)
        self.stream_action = tf.layers.flatten(self.streamA)
        self.stream_value = tf.layers.flatten(self.streamV)

        self.dense_action = tf.layers.dense(self.stream_action, num_actions, use_bias=False)
        self.dense_value = tf.layers.dense(self.stream_value, 1, use_bias=False)

        self.output = tf.add(self.dense_value,
                             tf.subtract(self.dense_action,
                                         tf.reduce_mean(self.dense_action, axis=1, keep_dims=True)))

        self.t_input = tf.placeholder(tf.float32, shape=(None, 84, 84, 3))

        self.t_c1 = tf.layers.conv2d(self.t_input, 32, 8, 4, use_bias=False, activation=tf.nn.relu)
        self.t_c2 = tf.layers.conv2d(self.t_c1, 64, 4, 2, use_bias=False, activation=tf.nn.relu)
        self.t_c3 = tf.layers.conv2d(self.t_c2, 64, 3, 1, use_bias=False, activation=tf.nn.relu)
        self.t_c4 = tf.layers.conv2d(self.t_c3, 512, 7, 1, use_bias=False, activation=tf.nn.relu)

        self.t_streamA, self.t_streamV = tf.split(self.t_c4, 2, 3)
        self.t_stream_action = tf.layers.flatten(self.t_streamA)
        self.t_stream_value = tf.layers.flatten(self.t_streamV)

        self.t_dense_action = tf.layers.dense(self.t_stream_action, num_actions, use_bias=False)
        self.t_dense_value = tf.layers.dense(self.t_stream_value, 1, use_bias=False)

        self.t_output = tf.add(self.t_dense_value,
                             tf.subtract(self.t_dense_action,
                                         tf.reduce_mean(self.t_dense_action, axis=1, keep_dims=True)))


    def __init__(self, num_actions, optimizer, future_discount, tau):
        # TF remembers everything you defined, this will keep the computation graph clean
        self.tau = tau
        self.__define_architecture(num_actions)
        self.upc1 = None

        self.optimizer = optimizer
        # stacked last 4 frames of the game

        self.reward_pl = tf.placeholder(tf.float32)
        self.done_pl = tf.placeholder(tf.float32)
        self.actions_pl = tf.placeholder(tf.int32, shape=[None])
        self.doubleq_pl = tf.placeholder(tf.float32, shape=[None])
        self.future_discount = tf.constant(future_discount)

        # all there is to double dqn
        # check
        self.reduce_targets = tf.argmax(self.output, axis=1)

        self.bool_hot = tf.one_hot(self.reduce_targets, num_actions, True, False)
        self.target_vals = tf.boolean_mask(self.t_output, self.bool_hot)

        #         yj = rj + done * gammma * maximum
        self.yj = tf.add(self.reward_pl,
                         tf.multiply(self.future_discount,
                                     tf.multiply(self.done_pl, self.doubleq_pl)
                                     )
                         )
        self.hots = tf.one_hot(self.actions_pl, num_actions, 1.0, 0.0, dtype=tf.float32)

        # self.yj_exp = tf.expand_dims(self.yj_pl, axis=1)
        # self.yj_mul = tf.concat([self.yj_exp for i in range(num_actions)], axis=1)
        # self.yj_masked = tf.multiply(self.yj_mul, self.hots)
        # self.predictions = tf.multiply(self.output, self.hots)
        self.predictions = tf.reduce_sum(tf.multiply(self.output, self.hots), axis=1)
        # self.loss = tf.losses.mean_squared_error(self.yj_masked, self.predictions)
        self.loss = tf.losses.mean_squared_error(self.yj, self.predictions)
        self.avg_q = tf.reduce_mean(self.output)
        self.train = optimizer.minimize(self.loss)

    def update_target_network(self, sess):
        sess.run(self.up_ops)
