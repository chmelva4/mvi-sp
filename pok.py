import tensorflow as tf
import gym
import numpy as np
from skimage.transform import resize
import cv2

def bgr2gray(arr):
    g = np.dot(arr[..., :3], [0.299, 0.587, 0.114])
    # print(g)
    return g

def downsample(img, oshape):
    return resize(img, oshape)

def crop(img, x1, x2, y1, y2):
    return img[x1:x2, y1:y2]

def theta(img):
    a = bgr2gray(img)
    b = downsample(a, (100, 84))
    c = crop(b, 16, 100, 0, b.shape[1])
    return c


if __name__ == '__main__':

    a = np.zeros((2,2))
    b = np.ones((2,2))
    c = np.arange(2, 6).reshape((2,2))

    frames = np.ones((1000000, 84, 84), dtype=np.uint8)
    mem = []
    # for i in range(1000000):
    #     n = np.zeros((84,84), dtype=np.uint8)
    #     frames[i] = n
    #     a = (frames[i], frames[i-1], frames[i-2], frames[i-3])
    #     mem.append(a)
    # print(len(mem))
    # input("Press Enter to continue...")
    env = gym.make('Breakout-v0')
    a = env.reset()
    print(env.unwrapped.get_action_meanings())

    l = [x for x in range(10)]
    print([x for x ])
    #
    # gray = env.reset()
    # gray = theta(gray)
    # gray = gray.astype(np.uint8)
    # cv2.imshow('result', gray), cv2.waitKey(0)
    # cv2.destroyAllWindows()
