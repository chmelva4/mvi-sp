import random
import numpy as np


class Experience:
    def __init__(self, s0, a, r, d, s1):
        self.s0 = s0
        self.a = a
        self.r = r
        self.d = d
        self.s1 = s1


class ReplayMemory:
    def __init__(self, size):
        self.size = size
        self.memory = [None for x in range(size)]
        self.h = 0
        self.fh = 0
        self.frames = np.zeros((self.size, 84, 84), np.uint8)
        self.occ = 0

    def append_frame(self, frame):
        self.frames[self.fh] = frame
        self.fh += 1
        if self.fh == self.size:
            self.fh = 0

    def get_last_4_frames(self):
        return self.frames[self.fh - 1], self.frames[self.fh - 2], self.frames[self.fh - 3], self.frames[self.fh - 4]

    def append(self, exp):
        self.memory[self.h] = exp
        self.h += 1
        if self.occ < self.size:
            self.occ += 1
        if self.h == self.size:
            self.h = 0

    def sample_memories(self, batch_size):
        return random.sample(self.memory[0:self.occ], batch_size)

class ReplayMemoryGridworld:

    def __init__(self, size):
        self.size = size
        self.memory = [None for x in range(size)]
        self.h = 0
        self.occ = 0

    def append(self, exp):
        self.memory[self.h] = exp
        self.h += 1
        if self.occ < self.size:
            self.occ += 1
        if self.h == self.size:
            self.h = 0

    def sample_memories(self, batch_size):
        return random.sample(self.memory[0:self.occ], batch_size)